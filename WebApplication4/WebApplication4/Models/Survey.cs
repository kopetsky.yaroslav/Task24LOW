﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication4.Models
{
    public class Survey
    {
        private bool LikedCheckbox;
        private string SelectedAuthor;


       public void MapToModel(FormCollection collection)
        {
            LikedCheckbox = Convert.ToBoolean(collection["Like"].Split(',')[0]);

            SelectedAuthor = collection["Drop"];
        }

       public string GetResultToString()
        {
            return "Result: Liked Our Page" + LikedCheckbox + " " + "Selected Author: " + SelectedAuthor; 
        }
    }
}