﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    public class SurveyController : Controller
    {
        Survey SurveyResult = new Survey();
        // GET: Survey
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public string Post(FormCollection collection)
        {
            SurveyResult.MapToModel(collection);

            return SurveyResult.GetResultToString();
        }
    }
}